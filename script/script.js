let tabTitle = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.tab-content');

for(let item of tabTitle){
    item.addEventListener('click', function(){
        for(let el of tabContent){
            el.classList.add('hidden');
        }
    
    let content = document.querySelector('#' + item.dataset.tab);
    content.classList.remove('hidden');

    tabTitle.forEach(elem => {
        elem.classList.remove('active');
    })
    this.classList.add('active');

});
}

